package forem

import (
	"bytes"
	"encoding/json"
	"strings"
	"time"
)

type stringTime time.Time

func (t *stringTime) UnmarshalJSON(p []byte) error {
	if bytes.Equal(p, []byte(`""`)) || bytes.Equal(p, []byte(`null`)) {
		*t = stringTime{}
		return nil
	}
	var target time.Time
	err := json.Unmarshal(p, &target)
	*t = stringTime(target)
	return err
}

// Some endpoints return tags as a string and tags_list as an array, others
// swap this.  WTF?!  This type handles both.
type tags []string

func (t *tags) UnmarshalJSON(p []byte) error {
	if p[0] == '"' {
		var target string
		err := json.Unmarshal(p, &target)
		if err != nil {
			return err
		}
		*t = strings.Split(strings.ReplaceAll(target, " ", ""), ",")
		return nil
	}
	var target []string
	err := json.Unmarshal(p, &target)
	*t = target
	return err
}

package forem

// User represents a Forem user.
type User struct {
	Name            string  `json:"name"`
	Username        string  `json:"username"`
	TwitterUsername *string `json:"twitter_username"`
	GithubUsername  *string `json:"github_username"`
	UserID          int     `json:"user_id"`
	WebsiteURL      *string `json:"sebsite_url"`
	ProfileImage    string  `json:"profile_image"`
	ProfileImage90  string  `json:"profile_image_90"`
}

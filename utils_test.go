package forem

import (
	"encoding/json"
	"net/http"
	"os"
	"testing"
	"time"

	"gitlab.com/flimzy/testy"
)

func parseTime(str string) time.Time {
	t, _ := time.Parse(time.RFC3339Nano, str)
	return t
}

func parseTimePtr(str string) *time.Time {
	t := parseTime(str)
	return &t
}

func liveClient(t *testing.T) *Client {
	t.Helper()

	base, key := os.Getenv("FOREM_API_URL"), os.Getenv("FOREM_API_KEY")
	if base == "" || key == "" {
		t.Skip("FOREM_API_URL and FOREM_API_KEY must be set for live tests")
	}
	c, err := New(base, key)
	if err != nil {
		t.Fatal(err)
	}
	return c
}

func testClient(t *testing.T, r testy.HTTPResponder) *Client {
	t.Helper()
	c, err := New("", "")
	if err != nil {
		t.Fatal(err)
	}
	c.SetHTTPClient(testy.HTTPClient(r))
	return c
}

func newResponse(status int, i any) *http.Response {
	switch t := i.(type) {
	case string:
		i = json.RawMessage(t)
	case []byte:
		i = json.RawMessage(t)
	}
	return &http.Response{
		StatusCode: status,
		Body:       jsonReader(i),
	}
}

package forem

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"sort"
	"strconv"
	"strings"
	"time"
)

// Frontmatter represents the frontmatter for an article.
type Frontmatter struct {
	Title        string    `yaml:"title"`
	Description  string    `yaml:"description"`
	Series       string    `yaml:"series"`
	Published    bool      `yaml:"published"`
	Date         time.Time `yaml:"date"`
	Tags         []string  `yaml:"tags"`
	CanonicalURL string    `yaml:"canonical_url"`
	CoverImage   string    `yaml:"cover_image"`
	// Extra are any unrecognized frontmatter values.
	Extra map[string]string
}

const fronmatterDateFormat = "2006-01-02 15:04:05 MST"

// Encode marshals Frontmatter using Forem's peculiar YAML-ish format.
func (f *Frontmatter) Encode() []byte {
	if f == nil {
		return nil
	}
	buf := &bytes.Buffer{}
	if len(f.Title) > 0 {
		fmt.Fprintf(buf, "title: %s\n", f.Title)
	}
	if len(f.Description) > 0 {
		fmt.Fprintf(buf, "description: %s\n", f.Description)
	}
	if len(f.Series) > 0 {
		fmt.Fprintf(buf, "series: %s\n", f.Series)
	}
	fmt.Fprintf(buf, "published: %t\n", f.Published)
	if !f.Date.IsZero() {
		fmt.Fprintf(buf, "date: %s\n", f.Date.Format(fronmatterDateFormat))
	}
	if len(f.Tags) > 0 {
		fmt.Fprintf(buf, "tags: %s\n", strings.Join(f.Tags, ", "))
	}
	if len(f.CanonicalURL) > 0 {
		fmt.Fprintf(buf, "canonical_url: %s\n", f.CanonicalURL)
	}
	if len(f.CoverImage) > 0 {
		fmt.Fprintf(buf, "cover_image: %s\n", f.CoverImage)
	}
	if len(f.Extra) > 0 {
		keys := make([]string, 0, len(f.Extra))
		for key := range f.Extra {
			keys = append(keys, key)
		}
		sort.Strings(keys)
		for _, key := range keys {
			fmt.Fprintf(buf, "%s: %s\n", key, f.Extra[key])
		}
	}

	return buf.Bytes()
}

// unmarshalYAML handles the frontmatter unmarshaling, since Forem doesn't
// conform to the same YAML parsing rules as the yaml.v3 library. In essence,
// this handles completely custom unmarshaling.
func (f *Frontmatter) unmarshalYAML(in io.Reader) error {
	s := bufio.NewScanner(in)
	lineCount := 0
	for s.Scan() {
		lineCount++
		line := s.Bytes()
		parts := bytes.SplitN(line, []byte(": "), 2)
		if len(parts) < 2 {
			return fmt.Errorf("unparseable frontmatter: %q at line %d", string(line), lineCount)
		}
		key := string(bytes.Trim(parts[0], `"`))
		switch key {
		case "title":
			f.Title = string(parts[1])
		case "description":
			f.Description = string(parts[1])
		case "series":
			f.Series = string(parts[1])
		case "published":
			var err error
			f.Published, err = strconv.ParseBool(string(parts[1]))
			if err != nil {
				return fmt.Errorf("field %q at line %d: %w", key, lineCount, err)
			}
		case "date":
			var err error
			f.Date, err = time.Parse(fronmatterDateFormat, string(parts[1]))
			if err != nil {
				return fmt.Errorf("field %q at line %d: %w", key, lineCount, err)
			}
		case "tags":
			if len(parts[1]) > 0 {
				tags := bytes.Split(parts[1], []byte(","))
				f.Tags = make([]string, len(tags))
				for i, tag := range tags {
					f.Tags[i] = string(bytes.TrimSpace(tag))
				}
			}
		case "canonical_url":
			f.CanonicalURL = string(parts[1])
		case "cover_image":
			f.CoverImage = string(parts[1])
		default:
			if f.Extra == nil {
				f.Extra = map[string]string{key: string(parts[1])}
				continue
			}
			f.Extra[key] = string(parts[1])
		}
	}
	return nil
}

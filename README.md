[![Go Report Card](https://goreportcard.com/badge/gitlab.com/flimzy/forem)](https://goreportcard.com/report/gitlab.com/flimzy/forem)
[![GoDoc](https://godoc.org/gitlab.com/flimzy/forem?status.svg)](https://pkg.go.dev/gitlab.com/flimzy/forem)

# Forem client SDK

package forem

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"net/url"
	"path/filepath"

	"gitlab.com/flimzy/ale/errors"
	"gitlab.com/flimzy/urlquery"
)

const acceptHeader = "application/vnd.forem.api-v1+json"

// Client represents a connection to a Forem instance.
type Client struct {
	apiKey  string
	client  *http.Client
	baseURL *url.URL
	dbg     bool
}

// New returns a new client instance with a default HTTP client.
func New(baseURL, apiKey string) (*Client, error) {
	base, err := url.Parse(baseURL)
	if err != nil {
		return nil, err
	}
	return &Client{
		apiKey:  apiKey,
		client:  &http.Client{},
		baseURL: base,
	}, nil
}

// SetHTTPClient allows setting a custom HTTP client to be used for making
// requests.
func (c *Client) SetHTTPClient(client *http.Client) {
	c.client = client
}

type path struct {
	*url.URL
	values interface{}
}

func (c *Client) path(parts ...any) *path {
	base := new(url.URL)
	*base = *c.baseURL
	components := append(make([]string, 0, len(parts)+1), base.Path)
	strParts := make([]string, len(parts))
	for i, part := range parts {
		strParts[i] = fmt.Sprint(part)
	}
	components = append(components, strParts...)
	base.Path = filepath.Join(components...)
	return &path{
		URL: base,
	}
}

var queryEncoding = urlquery.Encoding{
	ArrayMode: urlquery.ArrayCSV,
}

func (p *path) query(i interface{}) *path {
	p.values = i
	return p
}

func (p *path) String() (string, error) {
	params, err := queryEncoding.Encode(p.values)
	if err != nil {
		return "", err
	}
	p.RawQuery = params.Encode()
	return p.URL.String(), nil
}

func (c *Client) debug(format string, args ...interface{}) {
	if c.dbg {
		fmt.Printf(format, args...)
	}
}

func (c *Client) doRequest(ctx context.Context, method string, url *path, body, result any) error {
	path, err := url.String()
	if err != nil {
		return err
	}
	var bodyReader io.Reader
	if body != nil {
		switch method {
		case http.MethodGet, http.MethodHead:
			return errors.Errorf("body not permitted for method %s", method)
		}
		bodyReader = jsonReader(body)
	}
	req, err := http.NewRequestWithContext(ctx, method, path, bodyReader)
	if err != nil {
		return err
	}
	req.Header.Set("Api-Key", c.apiKey)
	req.Header.Set("Accept", acceptHeader)
	if bodyReader != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	c.debug("Request:\n%+v\n", &request{req})

	res, err := c.client.Do(req)
	if err != nil {
		return err
	}
	if res.StatusCode >= http.StatusBadRequest {
		errorResponse := new(foremError)
		if err := json.NewDecoder(res.Body).Decode(errorResponse); err != nil {
			return errors.WithHTTPStatus(err, http.StatusBadGateway)
		}
		return errorResponse
	}
	c.debug("Response:\n%+v\n", &response{res})
	defer res.Body.Close()
	return json.NewDecoder(res.Body).Decode(result)
}

type foremError struct {
	Message string `json:"error"`
	Status  int    `json:"status"`
}

func (e *foremError) Error() string {
	return e.Message
}

func (e *foremError) HTTPStatus() int {
	return e.Status
}

type response struct {
	*http.Response
}

func (r *response) Format(f fmt.State, _ rune) {
	withBody := false
	if f.Flag('+') {
		body, _ := io.ReadAll(r.Body)
		_ = r.Body.Close()
		r.Body = io.NopCloser(bytes.NewReader(body))
		defer func() {
			r.Body = io.NopCloser(bytes.NewReader(body))
		}()
		withBody = true
	}
	dump, _ := httputil.DumpResponse(r.Response, withBody)
	_, _ = f.Write(dump)
}

type request struct {
	*http.Request
}

func (r *request) Format(f fmt.State, _ rune) {
	withBody := false
	if f.Flag('+') && r.Body != nil {
		body, _ := io.ReadAll(r.Body)
		_ = r.Body.Close()
		r.Body = io.NopCloser(bytes.NewReader(body))
		defer func() {
			r.Body = io.NopCloser(bytes.NewReader(body))
		}()
		withBody = true
	}
	dump, _ := httputil.DumpRequest(r.Request, withBody)
	_, _ = f.Write(dump)
}

func jsonReader(i any) io.ReadCloser {
	r, w := io.Pipe()
	go func() {
		err := json.NewEncoder(w).Encode(i)
		w.CloseWithError(err)
	}()
	return r
}

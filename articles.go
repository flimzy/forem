package forem

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strings"
	"time"
)

// PaginationQuery are the query options to change pagination of results.
type PaginationQuery struct {
	// Page is the pagination page requested.
	Page int32 `url:"page,omitempty"`
	// PerPage is the page size (items to return per page). Defaults to 30.
	PerPage int32 `url:"per_page,omitempty"`
}

// Article represents a Forem article.
type Article struct {
	ID int `json:"id"`

	// The following fields may be modified via the API, as well as read.
	Title        string       `json:"title,omitempty"`
	Description  string       `json:"description,omitempty"`
	Frontmatter  *Frontmatter `json:"-"`
	Body         string       `json:"-"`
	CanonicalURL string       `json:"canonical_url,omitempty"`
	Tags         []string     `json:"tags,omitempty"`

	// These fields are write-only.
	Published      bool   `json:"published,omitempty"`
	Series         string `json:"series,omitempty"`
	MainImage      string `json:"main_image,omitempty"`
	OrganizationID int32  `json:"organization_id,omitempty"`

	// The remaining fields are read-only via the API. Some may be set via
	// front matter in Body
	ReadablePublishDate    string        `json:"readable_publish_date"`
	Slug                   string        `json:"slug"`
	Path                   string        `json:"path"`
	URL                    string        `json:"url"`
	CommentsCount          int           `json:"comments_count"`
	CollectionID           *int          `json:"collection_id"`
	PublishedTimestamp     time.Time     `json:"published_timestamp"`
	PositiveReactionsCount int           `json:"positive_reactions_count"`
	CoverImage             string        `json:"cover_image"`
	SocialImage            string        `json:"social_image"`
	CreatedAt              time.Time     `json:"created_at"`
	EditedAt               *time.Time    `json:"edited_at"`
	CrosspostedAt          *time.Time    `json:"crossposted_at"`
	PublishedAt            time.Time     `json:"published_at"`
	LastCommentAt          *time.Time    `json:"last_comment_at"`
	ReadingTimeMinutes     int           `json:"reading_time_minutes"`
	User                   *User         `json:"user"`
	Organization           *Organization `json:"organization"`
	FlareTag               *FlareTag     `json:"flare_tag"`
	PageViewsCount         int           `json:"page_views_count"`
	PublicReactionsCount   int           `json:"public_reactions_count"`
}

// UnmarshalJSON implements json.Unmarshaler.
func (a *Article) UnmarshalJSON(p []byte) error {
	target := struct {
		*Article
		Tags               tags       `json:"tags"`
		PublishedTimestamp stringTime `json:"published_timestamp"`
		MarkdownBody       string     `json:"body_markdown"`
		UnmarshalJSON      struct{}
	}{
		Article: a,
	}
	err := json.Unmarshal(p, &target)
	if err != nil {
		return err
	}
	a.Tags = target.Tags
	a.PublishedTimestamp = time.Time(target.PublishedTimestamp)
	bodyParts := strings.SplitN(target.MarkdownBody, yamlSeparator, 3)
	if len(bodyParts) == 3 {
		a.Body = strings.TrimLeft(bodyParts[2], "\n")
		a.Frontmatter = new(Frontmatter)
		if err := a.Frontmatter.unmarshalYAML(strings.NewReader(bodyParts[1])); err != nil {
			return err
		}
	} else {
		a.Body = target.MarkdownBody
	}
	return nil
}

const yamlSeparator = "---\n"

// MarshalJSON impements json.Marshaler.
func (a *Article) MarshalJSON() ([]byte, error) {
	type article struct {
		// Occlude the ID
		ID int `json:"-"`

		// Only these fields should be marshaled
		Title          string       `json:"title,omitempty"`
		Description    string       `json:"description,omitempty"`
		Frontmatter    *Frontmatter `json:"-"`
		Body           string       `json:"-"`
		CanonicalURL   string       `json:"canonical_url,omitempty"`
		Tags           []string     `json:"tags,omitempty"`
		Published      bool         `json:"published,omitempty"`
		Series         string       `json:"series,omitempty"`
		MainImage      string       `json:"main_image,omitempty"`
		OrganizationID int32        `json:"organization_id,omitempty"`

		// Occlude the remaining fields
		ReadablePublishDate    string        `json:"-"`
		Slug                   string        `json:"-"`
		Path                   string        `json:"-"`
		URL                    string        `json:"-"`
		CommentsCount          int           `json:"-"`
		CollectionID           *int          `json:"-"`
		PublishedTimestamp     time.Time     `json:"-"`
		PositiveReactionsCount int           `json:"-"`
		CoverImage             string        `json:"-"`
		SocialImage            string        `json:"-"`
		CreatedAt              time.Time     `json:"-"`
		EditedAt               *time.Time    `json:"-"`
		CrosspostedAt          *time.Time    `json:"-"`
		PublishedAt            time.Time     `json:"-"`
		LastCommentAt          *time.Time    `json:"-"`
		ReadingTimeMinutes     int           `json:"-"`
		User                   *User         `json:"-"`
		Organization           *Organization `json:"-"`
		FlareTag               *FlareTag     `json:"-"`
		PageViewsCount         int           `json:"-"`
		PublicReactionsCount   int           `json:"-"`
	}
	var markdownBody string
	if fm := a.Frontmatter.Encode(); len(fm) > 0 {
		markdownBody = yamlSeparator + string(fm) + yamlSeparator + "\n"
	}
	if len(a.Body) > 0 {
		markdownBody += a.Body
	}
	return json.Marshal(struct {
		article
		MarkdownBody string `json:"markdown_body,omitempty"`
	}{
		article:      article(*a),
		MarkdownBody: markdownBody,
	})
}

// ArticlesQuery are the query parameters possible when fetching a list of
// articles.
type ArticlesQuery struct {
	// Page is the pagination page requested.
	Page int32 `url:"page,omitempty"`
	// PerPage is the page size (items to return per page). Defaults to 30.
	PerPage      int32    `url:"per_page,omitempty"`
	Tag          string   `url:"tag,omitempty"`
	Tags         []string `url:"tags,omitempty"`
	TagsExclude  []string `url:"tags_exclude,omitempty"`
	Username     string   `url:"username,omitempty"`
	State        string   `url:"state,omitempty"`
	Top          int32    `url:"top,omitempty"`
	CollectionID int32    `url:"collection_id,omitempty"`
}

// Articles retrieves a list of articles.
//
// "Articles" are all the posts that users create on Forem that typically show
// up in the feed. They can be a blog post, a discussion question, a help
// thread etc. but is referred to as article within the code.
//
// By default it will return featured, published articles ordered by descending
// popularity.
//
// It supports pagination, and each page will contain 30 articles by default.
//
// See https://developers.forem.com/api/v1#tag/articles/operation/getArticles
func (c *Client) Articles(ctx context.Context, query *ArticlesQuery) ([]*Article, error) {
	var result []*Article
	err := c.doRequest(ctx, http.MethodGet, c.path("articles").query(query), nil, &result)
	return result, err
}

// LatestArticles retrieves a list of articles. ordered by descending publish
// date.
//
// See https://developers.forem.com/api/v0#tag/articles/operation/getLatestArticles
func (c *Client) LatestArticles(ctx context.Context, query *PaginationQuery) ([]*Article, error) {
	return c.articles(ctx, "articles", query)
}

func (c *Client) articles(ctx context.Context, path string, query *PaginationQuery) ([]*Article, error) {
	var result []*Article
	err := c.doRequest(ctx, http.MethodGet, c.path(path).query(query), nil, &result)
	return result, err
}

// CreateArticle creates a new article.
//
// See https://developers.forem.com/api/v0#tag/articles/operation/createArticle
func (c *Client) CreateArticle(ctx context.Context, article *Article) (*Article, error) {
	if article == nil {
		return nil, errors.New("article required")
	}
	body := struct {
		Article *Article `json:"article"`
	}{
		Article: article,
	}
	result := new(Article)
	err := c.doRequest(ctx, http.MethodPost, c.path("articles"), body, result)
	return result, err
}

// UpdateArticle updates an existing article.
//
// article.ID must be set.
//
// See https://developers.forem.com/api/v0#tag/articles/operation/updateArticle
func (c *Client) UpdateArticle(ctx context.Context, article *Article) (*Article, error) {
	if article == nil {
		return nil, errors.New("article required")
	}
	if article.ID == 0 {
		return nil, errors.New("article.ID required")
	}
	body := struct {
		Article *Article `json:"article"`
	}{
		Article: article,
	}
	result := new(Article)
	err := c.doRequest(ctx, http.MethodPut, c.path("articles", article.ID), body, result)
	return result, err
}

// Article retrieves a single published article given its id.
//
// See https://developers.forem.com/api/v0#tag/articles/operation/getArticleById
func (c *Client) Article(ctx context.Context, id int) (*Article, error) {
	if id == 0 {
		return nil, errors.New("id required")
	}
	result := new(Article)
	err := c.doRequest(ctx, http.MethodGet, c.path("articles", id), nil, result)
	return result, err
}

// ArticleBySlug retrieve a single published article given its username and slug.
//
// See https://developers.forem.com/api/v0#tag/articles/operation/getArticleByPath
func (c *Client) ArticleBySlug(ctx context.Context, username, slug string) (*Article, error) {
	if username == "" || slug == "" {
		return nil, errors.New("username and slug required")
	}
	result := new(Article)
	err := c.doRequest(ctx, http.MethodGet, c.path("articles", username, slug), nil, result)
	return result, err
}

// UsersArticles retrieves a list of published articles on behalf of an
// authenticated user.
//
// See https://developers.forem.com/api/v0#tag/articles/operation/getUserArticles
func (c *Client) UsersArticles(ctx context.Context, query *PaginationQuery) ([]*Article, error) {
	return c.articles(ctx, "articles/me", query)
}

// UsersPublishedArticles retrieves a list of published articles on behalf of
// an authenticated user.
//
// See https://developers.forem.com/api/v0#tag/articles/operation/getUserPublishedArticles
func (c *Client) UsersPublishedArticles(ctx context.Context, query *PaginationQuery) ([]*Article, error) {
	return c.articles(ctx, "articles/me/published", query)
}

// UsersUnpublishedArticles retrieves a list of unpublished articles on behalf
// of an authenticated user.
//
// See https://developers.forem.com/api/v0#tag/articles/operation/getUserUnpublishedArticles
func (c *Client) UsersUnpublishedArticles(ctx context.Context, query *PaginationQuery) ([]*Article, error) {
	return c.articles(ctx, "articles/me/unpublished", query)
}

// UsersAllArticles retrieves a list of all articles on behalf of an
// authenticated user.
//
// See https://developers.forem.com/api/v0#tag/articles/operation/getUserAllArticles
func (c *Client) UsersAllArticles(ctx context.Context, query *PaginationQuery) ([]*Article, error) {
	return c.articles(ctx, "articles/me/all", query)
}

// Video represents an article with a video.
type Video struct {
	ID                 int    `json:"id"`
	Path               string `json:"path"`
	CloudinaryVideoURL string `json:"cloudinary_video_url"`
	Title              string `json:"title"`
	UserID             int    `json:"user_id"`
	Duration           string `json:"video_duration_in_minutes"`
	SourceURL          string `json:"video_source_url"`
	User               *User  `json:"user"`
}

// Videos retrieves a list of articles that are uploaded with a video.
//
// See https://developers.forem.com/api/v0#tag/articles/operation/getArticlesWithVideo
func (c *Client) Videos(ctx context.Context, query *PaginationQuery) ([]*Video, error) {
	var result []*Video
	err := c.doRequest(ctx, http.MethodGet, c.path("videos").query(query), nil, &result)
	return result, err
}

package forem

import (
	"encoding/json"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/flimzy/testy"
)

func TestFrontmatter(t *testing.T) {
	type tt struct {
		body string
		want *Frontmatter
		err  string
	}

	tests := testy.NewTable()
	tests.Add("no body", tt{
		want: nil,
	})
	tests.Add("no frontmatter", tt{
		body: "This is a body, without any frontmatter.",
		want: nil,
	})
	tests.Add("invalid published value", tt{
		body: "---\npublished: whateva\n---\n\n",
		err:  `field "published" at line 1: strconv.ParseBool: parsing "whateva": invalid syntax`,
	})
	tests.Add("invalid date value", tt{
		body: "---\ndate: whateva\n---\n\n",
		err:  `field "date" at line 1: parsing time "whateva" as "2006-01-02 15:04:05 MST": cannot parse "whateva" as "2006"`,
	})
	tests.Add("cover_image", tt{
		body: "---\ncover_image: foo.jpg\n---\n\n",
		want: &Frontmatter{CoverImage: "foo.jpg"},
	})
	tests.Add("extra", tt{
		body: "---\nfoo: bar\nbar: baz\n---\n\n",
		want: &Frontmatter{Extra: map[string]string{
			"foo": "bar",
			"bar": "baz",
		}},
	})
	tests.Add("invalid frontmatter", tt{
		body: "---\ninvalid\n---\n\n",
		err:  `unparseable frontmatter: "invalid" at line 1`,
	})
	//nolint:revive // long lines permitted
	tests.Add("real body", tt{
		body: "---\ntitle: DevOps In A Blockchain World\npublished: false\ndate: 2022-10-20 00:00:00 UTC\ntags: \ncanonical_url: https://jhall.io/archive/2022/10/20/devops-in-a-blockchain-world/\n---\n\nThis week on Adventures in DevOps, Will and I talk with Vince Reed, Director of DevOps at Polygon Technology, a decentralized Ethereum scaling platform that enables developers to build scalable user-friendly dApps with low transaction fees without ever sacrificing security. They discuss how to build and scale a company that has grown from 40 to over 600 employees in 36 countries in the last year, implementing frameworks, communication, automation, security, and much more.",
		want: &Frontmatter{
			Title:        "DevOps In A Blockchain World",
			Date:         parseTime("2022-10-20T00:00:00Z"),
			CanonicalURL: "https://jhall.io/archive/2022/10/20/devops-in-a-blockchain-world/",
		},
	})
	tests.Add("tags", tt{
		body: `---
title: I hate vaporware
published: false
date: 2022-10-08 00:00:00 UTC
tags: estimates,sales,marketing,vaporware
canonical_url: https://jhall.io/archive/2022/10/08/i-hate-vaporware/
---
`,
		want: &Frontmatter{
			Title:        "I hate vaporware",
			Date:         parseTime("2022-10-08T00:00:00Z"),
			Tags:         []string{"estimates", "sales", "marketing", "vaporware"},
			CanonicalURL: "https://jhall.io/archive/2022/10/08/i-hate-vaporware/",
		},
	})
	tests.Add("mapping values error", tt{
		//nolint:revive // allow long lines
		body: `---
title: I hate vaporware
published: false
date: 2022-10-08 00:00:00 UTC
tags: estimates,sales,marketing,vaporware
canonical_url: https://jhall.io/archive/2022/10/08/i-hate-vaporware/
---

One reason [I didn’t mention yesterday](https://jhall.io/archive/2022/10/07/predictability-is-overrated/) for deadlines in software development is marketing and sales pressure.

I saved this one for a separate post, because I think it deserves its own special attention.

As the argument typically goes, we need to know when features X, Y, and Z will be ready, so that we can promise them to potential clients who may go to a competitor if we don’t make such promises.

Far be it from me to tell every sales and marketing department how to run their business, but in my opinion, it’s just irresponsible to make such claims, and likely unethical (and depending on your industry/production, possibly even immoral).

I have no problem with roadmaps, and transparency with (potential) clients about the order in which we plan to address on their requested feature. But these should not be communicated as promised delivery dates.

Instead of “Feature Y will be released in Q3”, I prefer “We expect to begin working on Feature Y in Q2” or even better: “Feature Y is item two on our roadmap, so we expect to begin working on it in the next 3-6 months.”

And this isn’t just me expressing a preference for flexible roadmaps (although I certainly do prefer that). This is me expressing my belief that reality is such that making such promises cannot be done honestly. [Software development is inherently unpredictable](https://jhall.io/archive/2022/02/18/software-development-is-inherently-unpredictable/), so any promises that ignore this are at best naive, and at worst, dishonest.

This is why I hate vaporware.

As a general rule, I much prefer to sell the product I have, when I have it.

* * *
_If you enjoyed this message, [subscribe](https://jhall.io/daily) to <u>The Daily Commit</u> to get future messages to your inbox._
`,
		want: &Frontmatter{
			Title:        "I hate vaporware",
			Date:         parseTime("2022-10-08T00:00:00Z"),
			Tags:         []string{"estimates", "sales", "marketing", "vaporware"},
			CanonicalURL: "https://jhall.io/archive/2022/10/08/i-hate-vaporware/",
		},
	})
	tests.Add("quote mark in string", tt{
		body: `---
title: "Transparency" is so opaque
published: false
date: 2022-08-18 00:00:00 UTC
tags: observability,culture
canonical_url: https://jhall.io/archive/2022/08/18/transparency-is-so-opaque/
---
`,
		want: &Frontmatter{
			Title:        `"Transparency" is so opaque`,
			Date:         parseTime("2022-08-18T00:00:00Z"),
			Tags:         []string{"observability", "culture"},
			CanonicalURL: "https://jhall.io/archive/2022/08/18/transparency-is-so-opaque/",
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		art, _ := json.Marshal(map[string]string{"body_markdown": tt.body})
		article := new(Article)
		err := json.Unmarshal(art, &article)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if err != nil {
			return
		}
		got := article.Frontmatter
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestArticle_marshal(t *testing.T) {
	type tt struct {
		article *Article
		want    string
	}

	tests := testy.NewTable()
	tests.Add("empty article", tt{
		article: &Article{},
		want:    `{}`,
	})
	tests.Add("full frontmatter", tt{
		article: &Article{
			Frontmatter: &Frontmatter{
				Title:        "title",
				Description:  "descr",
				Series:       "series",
				Published:    true,
				Date:         parseTime("2022-08-18T00:00:00Z"),
				Tags:         []string{"foo", "bar", "baz"},
				CanonicalURL: "https://example.com/",
				CoverImage:   "https://example.com/image.jpg",
				Extra: map[string]string{
					"foo": "bar",
					"bar": "baz",
				},
			},
		},
		//nolint:revive // permit long line
		want: `{
			"markdown_body": "---\ntitle: title\ndescription: descr\nseries: series\npublished: true\ndate: 2022-08-18 00:00:00 UTC\ntags: foo, bar, baz\ncanonical_url: https://example.com/\ncover_image: https://example.com/image.jpg\nbar: baz\nfoo: bar\n---\n\n"
		}`,
	})
	tests.Add("body and frontmatter ", tt{
		article: &Article{
			Frontmatter: &Frontmatter{
				Title: "title",
			},
			Body: "this is a test",
		},
		want: `{
			"markdown_body": "---\ntitle: title\npublished: false\n---\n\nthis is a test"
		}`,
	})
	tests.Add("body without frontmatter ", tt{
		article: &Article{
			Body: "this is a test",
		},
		want: `{
			"markdown_body": "this is a test"
		}`,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		got, err := json.Marshal(tt.article)
		if err != nil {
			t.Fatal(err)
		}
		if d := testy.DiffJSON(tt.want, got); d != nil {
			t.Error(d)
		}
	})
}

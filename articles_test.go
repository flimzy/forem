package forem

import (
	"context"
	"io"
	"net/http"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/flimzy/ale/errors"
	"gitlab.com/flimzy/testy"
)

func TestArticles_live(t *testing.T) {
	t.Parallel()

	t.Run("defaults", func(t *testing.T) {
		t.Parallel()
		c := liveClient(t)
		articles, err := c.Articles(context.Background(), nil)
		if err != nil {
			t.Fatal(err)
		}
		if len(articles) == 0 {
			t.Error("expected some results")
		}
	})
	t.Run("query", func(t *testing.T) {
		t.Parallel()
		c := liveClient(t)
		articles, err := c.Articles(context.Background(), &ArticlesQuery{
			PerPage:     2,
			Tags:        []string{"chicken"},
			TagsExclude: []string{"meta", "changelog"},
		})
		if err != nil {
			t.Fatal(err)
		}
		if len(articles) == 0 {
			t.Error("expected some results")
		}
	})
}

func TestArticles(t *testing.T) {
	type tt struct {
		responder testy.HTTPResponder
		query     *ArticlesQuery
		want      []*Article
		err       string
		status    int
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: func(*http.Request) (*http.Response, error) {
			return &http.Response{
				Header: http.Header{"Content-Type": {"application/json; charset=utf-8"}},
				//nolint:revive // permit long line
				Body: io.NopCloser(strings.NewReader(`[{"type_of":"article","id":1225176,"title":"Introducing the CodeSandbox GitHub App","description":"Learn how you can save hours every week reviewing PRs with the CodeSandbox GitHub App.","readable_publish_date":"Oct 20","slug":"introducing-the-codesandbox-github-app-50gi","path":"/codesandboxio/introducing-the-codesandbox-github-app-50gi","url":"https://dev.to/codesandboxio/introducing-the-codesandbox-github-app-50gi","comments_count":1,"public_reactions_count":4,"collection_id":null,"published_timestamp":"2022-10-20T13:58:45Z","positive_reactions_count":4,"cover_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--553M4FfX--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://codesandbox.io/static/introducing-the-codesandbox-github-app-d1cb40243f01790b8725a646f4b1f20f.png","social_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--u5FgOidW--/c_imagga_scale,f_auto,fl_progressive,h_500,q_auto,w_1000/https://codesandbox.io/static/introducing-the-codesandbox-github-app-d1cb40243f01790b8725a646f4b1f20f.png","canonical_url":"https://codesandbox.io/post/introducing-the-codesandbox-github-app","created_at":"2022-10-20T13:49:43Z","edited_at":"2022-10-20T13:58:55Z","crossposted_at":null,"published_at":"2022-10-20T13:58:45Z","last_comment_at":"2022-10-20T15:27:11Z","reading_time_minutes":5,"tag_list":["codesandbox","webdev","development"],"tags":"codesandbox, webdev, development","user":{"name":"Ives van Hoorne","username":"compuives","twitter_username":"CompuIves","github_username":"CompuIves","user_id":82063,"website_url":"https://twitter.com/compuives","profile_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--2iqebk02--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/82063/5afb0598-6fcc-4db2-9cd7-4d15fcadc7cc.jpeg","profile_image_90":"https://res.cloudinary.com/practicaldev/image/fetch/s--CBvEKycb--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/82063/5afb0598-6fcc-4db2-9cd7-4d15fcadc7cc.jpeg"},"organization":{"name":"CodeSandbox","username":"codesandboxio","slug":"codesandboxio","profile_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--ExPqSJKj--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/organization/profile_image/3772/2573f212-096d-4463-a7fe-fd0899bb8d6d.png","profile_image_90":"https://res.cloudinary.com/practicaldev/image/fetch/s--PqcDahEV--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/organization/profile_image/3772/2573f212-096d-4463-a7fe-fd0899bb8d6d.png"}},{"type_of":"article","id":1224549,"title":"We're blessed to work as Developers 🙏🏻🙏🏻","description":"Hi guys, I'm Kristian Lentino, 23 years old at the moment I'm writing this post and in these 2 days I...","readable_publish_date":"Oct 19","slug":"were-blessed-to-work-as-developers-fa2","path":"/kristianlentino99/were-blessed-to-work-as-developers-fa2","url":"https://dev.to/kristianlentino99/were-blessed-to-work-as-developers-fa2","comments_count":1,"public_reactions_count":6,"collection_id":null,"published_timestamp":"2022-10-19T20:18:33Z","positive_reactions_count":6,"cover_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--uL8pJVus--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/8roxgpsajz9yh3bha4ed.jpeg","social_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--kgHiHBT4--/c_imagga_scale,f_auto,fl_progressive,h_500,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/8roxgpsajz9yh3bha4ed.jpeg","canonical_url":"https://dev.to/kristianlentino99/were-blessed-to-work-as-developers-fa2","created_at":"2022-10-19T20:18:34Z","edited_at":null,"crossposted_at":null,"published_at":"2022-10-19T20:18:33Z","last_comment_at":"2022-10-20T17:56:38Z","reading_time_minutes":2,"tag_list":["community","career","devjournal"],"tags":"community, career, devjournal","user":{"name":"Kristian Lentino","username":"kristianlentino99","twitter_username":null,"github_username":"KristianLentino99","user_id":460103,"website_url":null,"profile_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--rvZAMpZP--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/460103/140af54b-6557-4fa5-a639-7444011d8459.png","profile_image_90":"https://res.cloudinary.com/practicaldev/image/fetch/s--VSOQb3tx--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/460103/140af54b-6557-4fa5-a639-7444011d8459.png"}}]`)),
			}, nil
		},
		//nolint:revive // allow long lines
		want: []*Article{
			{
				ID:                     1225176,
				Title:                  "Introducing the CodeSandbox GitHub App",
				Description:            "Learn how you can save hours every week reviewing PRs with the CodeSandbox GitHub App.",
				ReadablePublishDate:    "Oct 20",
				Slug:                   "introducing-the-codesandbox-github-app-50gi",
				Path:                   "/codesandboxio/introducing-the-codesandbox-github-app-50gi",
				URL:                    "https://dev.to/codesandboxio/introducing-the-codesandbox-github-app-50gi",
				CoverImage:             "https://res.cloudinary.com/practicaldev/image/fetch/s--553M4FfX--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://codesandbox.io/static/introducing-the-codesandbox-github-app-d1cb40243f01790b8725a646f4b1f20f.png",
				PublishedTimestamp:     parseTime("2022-10-20T13:58:45Z"),
				PositiveReactionsCount: 4,
				PublicReactionsCount:   4,
				CommentsCount:          1,
				EditedAt:               parseTimePtr("2022-10-20T13:58:55Z"),
				CanonicalURL:           "https://codesandbox.io/post/introducing-the-codesandbox-github-app",
				CreatedAt:              parseTime("2022-10-20T13:49:43Z"),
				SocialImage:            "https://res.cloudinary.com/practicaldev/image/fetch/s--u5FgOidW--/c_imagga_scale,f_auto,fl_progressive,h_500,q_auto,w_1000/https://codesandbox.io/static/introducing-the-codesandbox-github-app-d1cb40243f01790b8725a646f4b1f20f.png",
				PublishedAt:            parseTime("2022-10-20T13:58:45Z"),
				LastCommentAt:          parseTimePtr("2022-10-20T15:27:11Z"),
				ReadingTimeMinutes:     5,
				Tags:                   []string{"codesandbox", "webdev", "development"},
				User: &User{
					Name:            "Ives van Hoorne",
					Username:        "compuives",
					TwitterUsername: &[]string{"CompuIves"}[0],
					GithubUsername:  &[]string{"CompuIves"}[0],
					UserID:          82063,
					ProfileImage:    "https://res.cloudinary.com/practicaldev/image/fetch/s--2iqebk02--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/82063/5afb0598-6fcc-4db2-9cd7-4d15fcadc7cc.jpeg",
					ProfileImage90:  "https://res.cloudinary.com/practicaldev/image/fetch/s--CBvEKycb--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/82063/5afb0598-6fcc-4db2-9cd7-4d15fcadc7cc.jpeg",
				},
				Organization: &Organization{
					Name:           "CodeSandbox",
					Username:       "codesandboxio",
					Slug:           "codesandboxio",
					ProfileImage:   "https://res.cloudinary.com/practicaldev/image/fetch/s--ExPqSJKj--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/organization/profile_image/3772/2573f212-096d-4463-a7fe-fd0899bb8d6d.png",
					ProfileImage90: "https://res.cloudinary.com/practicaldev/image/fetch/s--PqcDahEV--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/organization/profile_image/3772/2573f212-096d-4463-a7fe-fd0899bb8d6d.png",
				},
			},
			{
				ID:                     1224549,
				Title:                  "We're blessed to work as Developers 🙏🏻🙏🏻",
				Description:            "Hi guys, I'm Kristian Lentino, 23 years old at the moment I'm writing this post and in these 2 days I...",
				ReadablePublishDate:    "Oct 19",
				Slug:                   "were-blessed-to-work-as-developers-fa2",
				Path:                   "/kristianlentino99/were-blessed-to-work-as-developers-fa2",
				URL:                    "https://dev.to/kristianlentino99/were-blessed-to-work-as-developers-fa2",
				LastCommentAt:          parseTimePtr("2022-10-20T17:56:38Z"),
				PublishedAt:            parseTime("2022-10-19T20:18:33Z"),
				ReadingTimeMinutes:     2,
				Tags:                   []string{"community", "career", "devjournal"},
				CommentsCount:          1,
				PublishedTimestamp:     parseTime("2022-10-19T20:18:33Z"),
				PositiveReactionsCount: 6,
				PublicReactionsCount:   6,
				CoverImage:             "https://res.cloudinary.com/practicaldev/image/fetch/s--uL8pJVus--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/8roxgpsajz9yh3bha4ed.jpeg",
				SocialImage:            "https://res.cloudinary.com/practicaldev/image/fetch/s--kgHiHBT4--/c_imagga_scale,f_auto,fl_progressive,h_500,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/8roxgpsajz9yh3bha4ed.jpeg",
				CreatedAt:              parseTime("2022-10-19T20:18:34Z"),
				CanonicalURL:           "https://dev.to/kristianlentino99/were-blessed-to-work-as-developers-fa2",
				User: &User{
					Name:           "Kristian Lentino",
					Username:       "kristianlentino99",
					GithubUsername: &[]string{"KristianLentino99"}[0],
					UserID:         460103,
					ProfileImage:   "https://res.cloudinary.com/practicaldev/image/fetch/s--rvZAMpZP--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/460103/140af54b-6557-4fa5-a639-7444011d8459.png",
					ProfileImage90: "https://res.cloudinary.com/practicaldev/image/fetch/s--VSOQb3tx--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/460103/140af54b-6557-4fa5-a639-7444011d8459.png",
				},
			},
		},
	})
	tests.Add("blank date", tt{
		responder: func(*http.Request) (*http.Response, error) {
			return &http.Response{
				Header: http.Header{"Content-Type": {"application/json; charset=utf-8"}},
				//nolint:revive // permit long line
				Body: io.NopCloser(strings.NewReader(`[{"type_of":"article","id":1225728,"title":"DevOps In A Blockchain World","description":"This week on Adventures in DevOps, Will and I talk with Vince Reed, Director of DevOps at Polygon...","published":false,"published_at":null,"slug":"devops-in-a-blockchain-world-4lb1-temp-slug-2144334","path":"/tinydevops/devops-in-a-blockchain-world-4lb1-temp-slug-2144334","url":"https://dev.to/tinydevops/devops-in-a-blockchain-world-4lb1-temp-slug-2144334","comments_count":0,"public_reactions_count":0,"page_views_count":0,"published_timestamp":"","body_markdown":"---\ntitle: DevOps In A Blockchain World\npublished: false\ndate: 2022-10-20 00:00:00 UTC\ntags: \ncanonical_url: https://jhall.io/archive/2022/10/20/devops-in-a-blockchain-world/\n---\n\nThis week on Adventures in DevOps, Will and I talk with Vince Reed, Director of DevOps at Polygon Technology, a decentralized Ethereum scaling platform that enables developers to build scalable user-friendly dApps with low transaction fees without ever sacrificing security. They discuss how to build and scale a company that has grown from 40 to over 600 employees in 36 countries in the last year, implementing frameworks, communication, automation, security, and much more.","positive_reactions_count":0,"cover_image":null,"tag_list":[],"canonical_url":"https://jhall.io/archive/2022/10/20/devops-in-a-blockchain-world/","reading_time_minutes":1,"user":{"name":"Jonathan Hall","username":"tinydevops","twitter_username":"DevOpsHabits","github_username":"flimzy","user_id":287061,"website_url":"https://jhall.io/","profile_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--hI0_G4rU--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/287061/ec33a650-03c8-4167-970b-8590903f76cb.jpg","profile_image_90":"https://res.cloudinary.com/practicaldev/image/fetch/s--7PYGpTSC--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/287061/ec33a650-03c8-4167-970b-8590903f76cb.jpg"}}]`)),
			}, nil
		},
		//nolint:revive // allow long lines
		want: []*Article{
			{
				ID:                 1225728,
				Title:              "DevOps In A Blockchain World",
				Description:        "This week on Adventures in DevOps, Will and I talk with Vince Reed, Director of DevOps at Polygon...",
				Slug:               "devops-in-a-blockchain-world-4lb1-temp-slug-2144334",
				Path:               "/tinydevops/devops-in-a-blockchain-world-4lb1-temp-slug-2144334",
				URL:                "https://dev.to/tinydevops/devops-in-a-blockchain-world-4lb1-temp-slug-2144334",
				ReadingTimeMinutes: 1,
				CanonicalURL:       "https://jhall.io/archive/2022/10/20/devops-in-a-blockchain-world/",
				Frontmatter: &Frontmatter{
					Title:        "DevOps In A Blockchain World",
					Published:    false,
					Date:         parseTime("2022-10-20T00:00:00Z"),
					CanonicalURL: "https://jhall.io/archive/2022/10/20/devops-in-a-blockchain-world/",
				},
				Body: "This week on Adventures in DevOps, Will and I talk with Vince Reed, Director of DevOps at Polygon Technology, a decentralized Ethereum scaling platform that enables developers to build scalable user-friendly dApps with low transaction fees without ever sacrificing security. They discuss how to build and scale a company that has grown from 40 to over 600 employees in 36 countries in the last year, implementing frameworks, communication, automation, security, and much more.",
				User: &User{
					Name:            "Jonathan Hall",
					Username:        "tinydevops",
					TwitterUsername: &[]string{"DevOpsHabits"}[0],
					GithubUsername:  &[]string{"flimzy"}[0],
					UserID:          287061,
					ProfileImage:    "https://res.cloudinary.com/practicaldev/image/fetch/s--hI0_G4rU--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/287061/ec33a650-03c8-4167-970b-8590903f76cb.jpg",
					ProfileImage90:  "https://res.cloudinary.com/practicaldev/image/fetch/s--7PYGpTSC--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/287061/ec33a650-03c8-4167-970b-8590903f76cb.jpg",
				},
			},
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		c := testClient(t, tt.responder)

		got, err := c.Articles(context.Background(), tt.query)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected error status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestLatestArticles_live(t *testing.T) {
	t.Parallel()

	t.Run("defaults", func(t *testing.T) {
		t.Parallel()
		c := liveClient(t)
		articles, err := c.LatestArticles(context.Background(), nil)
		if err != nil {
			t.Fatal(err)
		}
		if len(articles) == 0 {
			t.Error("expected some results")
		}
	})
	t.Run("query", func(t *testing.T) {
		t.Parallel()
		c := liveClient(t)
		articles, err := c.LatestArticles(context.Background(), &PaginationQuery{
			PerPage: 1,
		})
		if err != nil {
			t.Fatal(err)
		}
		if len(articles) == 0 {
			t.Error("expected some results")
		}
	})
}

func TestLatestArticles(t *testing.T) {
	type tt struct {
		responder testy.HTTPResponder
		query     *PaginationQuery
		want      []*Article
		err       string
		status    int
	}

	tests := testy.NewTable()
	tests.Add("success", tt{
		responder: func(*http.Request) (*http.Response, error) {
			return &http.Response{
				Header: http.Header{"Content-Type": {"application/json; charset=utf-8"}},
				//nolint:revive // permit long line
				Body: io.NopCloser(strings.NewReader(`[{"type_of":"article","id":1225176,"title":"Introducing the CodeSandbox GitHub App","description":"Learn how you can save hours every week reviewing PRs with the CodeSandbox GitHub App.","readable_publish_date":"Oct 20","slug":"introducing-the-codesandbox-github-app-50gi","path":"/codesandboxio/introducing-the-codesandbox-github-app-50gi","url":"https://dev.to/codesandboxio/introducing-the-codesandbox-github-app-50gi","comments_count":1,"public_reactions_count":4,"collection_id":null,"published_timestamp":"2022-10-20T13:58:45Z","positive_reactions_count":4,"cover_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--553M4FfX--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://codesandbox.io/static/introducing-the-codesandbox-github-app-d1cb40243f01790b8725a646f4b1f20f.png","social_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--u5FgOidW--/c_imagga_scale,f_auto,fl_progressive,h_500,q_auto,w_1000/https://codesandbox.io/static/introducing-the-codesandbox-github-app-d1cb40243f01790b8725a646f4b1f20f.png","canonical_url":"https://codesandbox.io/post/introducing-the-codesandbox-github-app","created_at":"2022-10-20T13:49:43Z","edited_at":"2022-10-20T13:58:55Z","crossposted_at":null,"published_at":"2022-10-20T13:58:45Z","last_comment_at":"2022-10-20T15:27:11Z","reading_time_minutes":5,"tag_list":["codesandbox","webdev","development"],"tags":"codesandbox, webdev, development","user":{"name":"Ives van Hoorne","username":"compuives","twitter_username":"CompuIves","github_username":"CompuIves","user_id":82063,"website_url":"https://twitter.com/compuives","profile_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--2iqebk02--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/82063/5afb0598-6fcc-4db2-9cd7-4d15fcadc7cc.jpeg","profile_image_90":"https://res.cloudinary.com/practicaldev/image/fetch/s--CBvEKycb--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/82063/5afb0598-6fcc-4db2-9cd7-4d15fcadc7cc.jpeg"},"organization":{"name":"CodeSandbox","username":"codesandboxio","slug":"codesandboxio","profile_image":"https://res.cloudinary.com/practicaldev/image/fetch/s--ExPqSJKj--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/organization/profile_image/3772/2573f212-096d-4463-a7fe-fd0899bb8d6d.png","profile_image_90":"https://res.cloudinary.com/practicaldev/image/fetch/s--PqcDahEV--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/organization/profile_image/3772/2573f212-096d-4463-a7fe-fd0899bb8d6d.png"}}]`)),
			}, nil
		},
		//nolint:revive // allow long lines
		want: []*Article{
			{
				ID:                     1225176,
				Title:                  "Introducing the CodeSandbox GitHub App",
				Description:            "Learn how you can save hours every week reviewing PRs with the CodeSandbox GitHub App.",
				ReadablePublishDate:    "Oct 20",
				Slug:                   "introducing-the-codesandbox-github-app-50gi",
				Path:                   "/codesandboxio/introducing-the-codesandbox-github-app-50gi",
				URL:                    "https://dev.to/codesandboxio/introducing-the-codesandbox-github-app-50gi",
				CoverImage:             "https://res.cloudinary.com/practicaldev/image/fetch/s--553M4FfX--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://codesandbox.io/static/introducing-the-codesandbox-github-app-d1cb40243f01790b8725a646f4b1f20f.png",
				PublishedTimestamp:     parseTime("2022-10-20T13:58:45Z"),
				PositiveReactionsCount: 4,
				PublicReactionsCount:   4,
				CommentsCount:          1,
				EditedAt:               parseTimePtr("2022-10-20T13:58:55Z"),
				CanonicalURL:           "https://codesandbox.io/post/introducing-the-codesandbox-github-app",
				CreatedAt:              parseTime("2022-10-20T13:49:43Z"),
				SocialImage:            "https://res.cloudinary.com/practicaldev/image/fetch/s--u5FgOidW--/c_imagga_scale,f_auto,fl_progressive,h_500,q_auto,w_1000/https://codesandbox.io/static/introducing-the-codesandbox-github-app-d1cb40243f01790b8725a646f4b1f20f.png",
				PublishedAt:            parseTime("2022-10-20T13:58:45Z"),
				LastCommentAt:          parseTimePtr("2022-10-20T15:27:11Z"),
				ReadingTimeMinutes:     5,
				Tags:                   []string{"codesandbox", "webdev", "development"},
				User: &User{
					Name:            "Ives van Hoorne",
					Username:        "compuives",
					TwitterUsername: &[]string{"CompuIves"}[0],
					GithubUsername:  &[]string{"CompuIves"}[0],
					UserID:          82063,
					ProfileImage:    "https://res.cloudinary.com/practicaldev/image/fetch/s--2iqebk02--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/82063/5afb0598-6fcc-4db2-9cd7-4d15fcadc7cc.jpeg",
					ProfileImage90:  "https://res.cloudinary.com/practicaldev/image/fetch/s--CBvEKycb--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/user/profile_image/82063/5afb0598-6fcc-4db2-9cd7-4d15fcadc7cc.jpeg",
				},
				Organization: &Organization{
					Name:           "CodeSandbox",
					Username:       "codesandboxio",
					Slug:           "codesandboxio",
					ProfileImage:   "https://res.cloudinary.com/practicaldev/image/fetch/s--ExPqSJKj--/c_fill,f_auto,fl_progressive,h_640,q_auto,w_640/https://dev-to-uploads.s3.amazonaws.com/uploads/organization/profile_image/3772/2573f212-096d-4463-a7fe-fd0899bb8d6d.png",
					ProfileImage90: "https://res.cloudinary.com/practicaldev/image/fetch/s--PqcDahEV--/c_fill,f_auto,fl_progressive,h_90,q_auto,w_90/https://dev-to-uploads.s3.amazonaws.com/uploads/organization/profile_image/3772/2573f212-096d-4463-a7fe-fd0899bb8d6d.png",
				},
			},
		},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		c := testClient(t, tt.responder)

		got, err := c.LatestArticles(context.Background(), tt.query)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected error status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestCreateArticle_live(t *testing.T) {
	t.Parallel()

	t.Run("defaults", func(t *testing.T) {
		t.Skip()
		t.Parallel()
		c := liveClient(t)
		c.dbg = true
		article, err := c.CreateArticle(context.Background(), &Article{
			Title: "test",
			Body:  "testing asdf",
		})
		if err != nil {
			t.Fatal(err)
		}
		if article.Title != "test" {
			t.Errorf("Unexpected title returned: %s", article.Title)
		}
	})
}

func TestCreateArticle(t *testing.T) {
	type tt struct {
		responder testy.HTTPResponder
		article   *Article
		want      *Article
		err       string
		status    int
	}

	tests := testy.NewTable()
	tests.Add("nil article", tt{
		err:    "article required",
		status: http.StatusInternalServerError,
	})
	tests.Add("empty article", tt{
		responder: func(*http.Request) (*http.Response, error) {
			return newResponse(http.StatusUnprocessableEntity, `{"error":"Body markdown has already been taken","status":422}`), nil
		},
		article: &Article{},
		err:     "Body markdown has already been taken",
		status:  http.StatusUnprocessableEntity,
	})
	tests.Add("success mimicry", tt{
		responder: func(*http.Request) (*http.Response, error) {
			return newResponse(http.StatusOK, `{}`), nil
		},
		article: &Article{},
		want:    &Article{},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		c := testClient(t, tt.responder)

		got, err := c.CreateArticle(context.Background(), tt.article)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected error status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func TestUpdateArticle(t *testing.T) {
	type tt struct {
		responder testy.HTTPResponder
		article   *Article
		want      *Article
		err       string
		status    int
	}

	tests := testy.NewTable()
	tests.Add("nil article", tt{
		err:    "article required",
		status: http.StatusInternalServerError,
	})
	tests.Add("empty article", tt{
		article: &Article{},
		err:     "article.ID required",
		status:  http.StatusInternalServerError,
	})
	tests.Add("success mimicry", tt{
		responder: func(*http.Request) (*http.Response, error) {
			return newResponse(http.StatusOK, `{}`), nil
		},
		article: &Article{
			ID: 123,
		},
		want: &Article{},
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		c := testClient(t, tt.responder)

		got, err := c.UpdateArticle(context.Background(), tt.article)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("unexpected error: %s", err)
		}
		if status := errors.InspectHTTPStatus(err); status != tt.status {
			t.Errorf("Unexpected error status: %v", status)
		}
		if err != nil {
			return
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}
